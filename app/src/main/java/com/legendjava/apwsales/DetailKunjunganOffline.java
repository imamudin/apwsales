package com.legendjava.apwsales;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.legendjava.apwsales.app.MyAppController;
import com.legendjava.apwsales.config.GlobalConfig;
import com.legendjava.apwsales.db.DBHelper;
import com.legendjava.apwsales.mysp.ObscuredSharedPreferences;
import com.legendjava.apwsales.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import static com.legendjava.apwsales.TokoOffline.decodeSampledBitmapFromFile;

/**
 * Created by imamudin on 12/05/18.
 */

public class DetailKunjunganOffline extends AppCompatActivity {
    ObscuredSharedPreferences pref;
    JsonObjectRequest request;

    LinearLayout ll_main;
    ProgressDialog loading;

    TextView tv_nama_toko,tv_kegiatan, tv_tgl_kirim, tv_total, tv_nama_gudang;
    ImageView img_toko;
    LinearLayout ll_order_offline, ll_data_order;
    Button btn_simpan_ke_server;

    int kunjungan_id, order_id;

    MyLog myLog;
    DBHelper mydb;

    ArrayList<HashMap<String, String>> al_kunjungan, al_order, al_order_detil, al_kegiatan;

    JSONArray json_id_produk, json_jumlah, json_harga;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_kunjungan_offline);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("DATA KUNJUNGAN");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref    = new ObscuredSharedPreferences(DetailKunjunganOffline.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog   = new MyLog(getLocalClassName());
        mydb    = new DBHelper(DetailKunjunganOffline.this);

        Intent old  = getIntent();
        kunjungan_id    = Integer.parseInt(old.getStringExtra(DBHelper.KUNJUNGAN_ID));

        ll_main         = (LinearLayout)findViewById(R.id.ll_main);

        tv_nama_toko            = (TextView) findViewById(R.id.tv_nama_toko);
        tv_kegiatan             = (TextView) findViewById(R.id.tv_kegiatan);
        tv_tgl_kirim            = (TextView) findViewById(R.id.tv_tgl_kirim);
        tv_total                = (TextView) findViewById(R.id.tv_total);
        tv_nama_gudang          = (TextView) findViewById(R.id.tv_nama_gudang);

        img_toko                = (ImageView) findViewById(R.id.img_toko);
        ll_order_offline        = (LinearLayout) findViewById(R.id.ll_order_offline);
        ll_data_order           = (LinearLayout) findViewById(R.id.ll_data_order);

        btn_simpan_ke_server    = (Button)findViewById(R.id.btn_simpan_ke_server);

        btn_simpan_ke_server.setOnClickListener(btnClick);

        displayView();
    }
    private void displayView(){
        al_kunjungan = mydb.getKunjunganById(kunjungan_id);
        al_order     = mydb.getOrderByKunjunganID(kunjungan_id);

        al_kegiatan  = mydb.getKegiatanByKunjunganID(kunjungan_id);

        json_id_produk  = new JSONArray();
        json_jumlah     = new JSONArray();
        json_harga      = new JSONArray();

        if(al_kunjungan.size() >= 1){
            tv_nama_toko.setText(al_kunjungan.get(0).get(DBHelper.KUNJUNGAN_NAMA_TOKO));
            add_gambar(al_kunjungan.get(0).get(DBHelper.KUNJUNGAN_IMAGE_PATH));
        }
        if(al_kegiatan.size() >= 1){
            tv_kegiatan.setText(al_kegiatan.get(0).get(DBHelper.KEGIATAN_KETERANGAN));
        }
        if(al_order.size() >= 1) {
            ll_data_order.setVisibility(View.VISIBLE);

            //data order detail
            order_id = Integer.parseInt(al_order.get(0).get(DBHelper.ORDER_ID));
            al_order_detil= mydb.getDetailOrderByOrderId(order_id);

            tv_tgl_kirim.setText(al_order.get(0).get(DBHelper.ORDER_TANGGAL_KIRIM_SHOW));
            tv_total.setText(decimalToRupiah(Double.parseDouble(al_order.get(0).get(DBHelper.ORDER_TOTAL_HARGA))));
            tv_nama_gudang.setText(al_order.get(0).get(DBHelper.ORDER_GUDANG_NAMA));

            HashMap<String, String> temp = new HashMap<String, String>();
            for (int i = 0; i < al_order_detil.size(); i++) {
                temp = al_order_detil.get(i);

                String keterangan = temp.get(DBHelper.OD_JUMLAH_PESANAN) +
                        " x " + decimalToRupiah(Double.parseDouble(temp.get(DBHelper.OD_HARGA)));
                double total = Double.parseDouble(temp.get(DBHelper.OD_JUMLAH_PESANAN)) * Double.parseDouble(temp.get(DBHelper.OD_HARGA));
                ll_order_offline.addView(getViewDataOrder(temp.get(DBHelper.OD_PRODUK_NAMA),
                        keterangan, decimalToRupiah(total)));

                try {
                    json_id_produk.put(i, temp.get(DBHelper.OD_ID_PRODUK));
                    json_harga.put(i,temp.get(DBHelper.OD_HARGA));
                    json_jumlah.put(i,temp.get(DBHelper.OD_JUMLAH_PESANAN));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else {
            order_id = 0;
            al_order_detil= mydb.getDetailOrderByOrderId(0);
        }
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String s_harga = kursIndonesia.format(harga);
        return s_harga.substring(0, (s_harga.length()-3));
    }
    private View getViewDataOrder(String nama, String jumlah, String total){
        LayoutInflater inflater     = (LayoutInflater)this.getLayoutInflater();
        View data_history_view    = inflater.inflate(R.layout.list_order_dialog, null);

        final TextView tv_nama_produk   =(TextView)data_history_view.findViewById(R.id.tv_nama_produk);
        final TextView tv_ketrangan     =(TextView)data_history_view.findViewById(R.id.tv_keterangan);
        final TextView tv_total_harga   =(TextView)data_history_view.findViewById(R.id.tv_total_harga);

        tv_nama_produk.setText(nama);
        tv_ketrangan.setText(jumlah);
        tv_total_harga.setText(total);

        return data_history_view;
    }
    private void add_gambar(final String path){
        Bitmap bitmapForImage = null;
        File file = new File(path);
        Log.d(GlobalConfig.TAG+"mycopcamera", path);
        if (file.exists()) {
            bitmapForImage = decodeSampledBitmapFromFile(path, 300, 300);
        }
        img_toko.setImageBitmap(bitmapForImage);
    }
    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==btn_simpan_ke_server){
                ConnectivityManager cm =
                        (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected){
                    simpanDetailKunjunganOffline();
                }else{
                    notifikasi("Silakan aktifkan sambungan internet.");
                }
            }
        }
    };
    private void simpanDetailKunjunganOffline(){
        Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "make progress dialog");
        loading = ProgressDialog.show(DetailKunjunganOffline.this, "Menyimpan data", "Mohon tunggu...", true);

        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_SINKRONISASI;

        myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();

            //data untuk privillage
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));       //mewaliki nik juga
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            //data untuk kunjungan
            jsonBody.put(GlobalConfig.ID_TOKO, al_kunjungan.get(0).get(DBHelper.KUNJUNGAN_KODE));
            jsonBody.put(GlobalConfig.GKUNJUNGAN_TGL, al_kunjungan.get(0).get(DBHelper.KUNJUNGAN_TANGGAL));
            jsonBody.put(GlobalConfig.GKUNJUNGAN_JAM_M, al_kunjungan.get(0).get(DBHelper.KUNJUNGAN_JAM_MULAI));
            jsonBody.put(GlobalConfig.GKUNJUNGAN_JAM_S, al_kunjungan.get(0).get(DBHelper.KUNJUNGAN_JAM_SELESAI));
            jsonBody.put(GlobalConfig.GTOKO_LAT, al_kunjungan.get(0).get(DBHelper.KUNJUNGAN_LATITUDE));
            jsonBody.put(GlobalConfig.GTOKO_LONG, al_kunjungan.get(0).get(DBHelper.KUNJUNGAN_LONGITUDE));
            jsonBody.put(DBHelper.KUNJUNGAN_IS_BARCODE, al_kunjungan.get(0).get(DBHelper.KUNJUNGAN_IS_BARCODE));


            //data untuk order
            if(al_order.size() >= 1) {
                BigDecimal bd = new BigDecimal(al_order.get(0).get(DBHelper.ORDER_TOTAL_HARGA));
                String total_harga = ""+bd.longValue();
                myLog.print("nilai total", total_harga);

                jsonBody.put(GlobalConfig.JENIS_PENGIRIMAN, al_order.get(0).get(DBHelper.ORDER_JENIS_PENGIRIMAN));
                jsonBody.put(GlobalConfig.GPRODUK_ID, json_id_produk);
                jsonBody.put(GlobalConfig.JUMLAH_PESANAN, json_jumlah);
                jsonBody.put(GlobalConfig.GHARGA, json_harga);


                jsonBody.put(GlobalConfig.TANGGAL_KIRIM, al_order.get(0).get(DBHelper.ORDER_TANGGAL_KIRIM));
                jsonBody.put(GlobalConfig.TOTAL_HARGA, total_harga);
                jsonBody.put(GlobalConfig.GKODE_GUDANG, al_order.get(0).get(DBHelper.ORDER_GUDANG));
                jsonBody.put(GlobalConfig.IS_APPROVAL, al_order.get(0).get(DBHelper.ORDER_IS_APPROVAL));
            }

            //data untuk kegiatan
            if(al_kegiatan.size() >= 1) {
                jsonBody.put("KEGIATAN", al_kegiatan.get(0).get(DBHelper.KEGIATAN_KETERANGAN));
            }

            //untuk gambar toko
            Bitmap bmp = decodeSampledBitmapFromFile(al_kunjungan.get(0).get(DBHelper.KUNJUNGAN_IMAGE_PATH), 1680, 960);
            jsonBody.put("FOTO", getStringImage(bmp));


            myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(),""+jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus kunjungan dari database lokal
                            mydb.deleteKunjunganById(kunjungan_id, order_id);
                            notifikasi_dialog("",message);
                        }else{
                            notifikasi(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 500:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }

    private void notifikasi_dialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailKunjunganOffline.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        setResult(RESULT_OK);
                        DetailKunjunganOffline.this.finish();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
