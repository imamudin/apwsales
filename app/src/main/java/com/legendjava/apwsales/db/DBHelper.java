package com.legendjava.apwsales.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {
    private static String DB_PATH = "/data/data/com.legendjava.apwsales/databases/";
    private static String DB_NAME = "apwsales.db";
    public static int LIMIT                     = 50;

    public static final String DATABASE_NAME            = "apwsales.db";
    public static final String TABLE_TOKO       = "TOKO";
    public static final String TOKO_ID          = "ID";
    public static final String TOKO_KODE        = "KODE";
    public static final String TOKO_NAMA        = "NAMA";
    public static final String TOKO_ALAMAT      = "ALAMAT";

    public static final String TABLE_KUNJUNGAN  = "KUNJUNGAN";
    public static final String KUNJUNGAN_LAST_ID     = "K_LAST_ID";     //hanya untuk preferences
    public static final String KUNJUNGAN_ID     = "K_ID";
    public static final String KUNJUNGAN_KODE   = "KODE"; //kode toko
    public static final String KUNJUNGAN_NAMA_TOKO  = "NAMA_TOKO"; //nama toko
    public static final String KUNJUNGAN_LATITUDE   = "LATITUDE";
    public static final String KUNJUNGAN_LONGITUDE  = "LONGITUDE";
    public static final String KUNJUNGAN_TANGGAL= "TANGGAL";    //2018-10-24
    public static final String KUNJUNGAN_JAM_MULAI  = "JAM_MULAI";  //09:46:10
    public static final String KUNJUNGAN_JAM_SELESAI= "JAM_SELESAI";
    public static final String KUNJUNGAN_IS_BARCODE = "IS_BARCODE";
    public static final String KUNJUNGAN_IMAGE_PATH = "GAMBAR";

    public static final String TABLE_ORDER      = "TORDER";
    public static final String ORDER_ID         = "O_ID";
    public static final String ORDER_K_ID       = "KUNJUNGAN_ID";
    public static final String ORDER_KODE_TOKO  = "KODE";
    public static final String ORDER_TOTAL_HARGA= "total_harga";
    public static final String ORDER_GUDANG     = "KODE_GUDANG";
    public static final String ORDER_GUDANG_NAMA= "NAMA_GUDANG";
    public static final String ORDER_IS_APPROVAL= "is_approval";
    public static final String ORDER_JENIS_PENGIRIMAN= "JENIS_PENGIRIMAN";
    public static final String ORDER_TANGGAL_KIRIM= "tanggal_kirim";
    public static final String ORDER_TANGGAL_KIRIM_SHOW= "tanggal_kirim_s";

    public static final String TABLE_ORDER_DETAIL= "ORDER_DETAIL";
    public static final String OD_ID            = "OD_ID";
    public static final String OD_ORDER_ID      = "ORDER_ID";
    public static final String OD_ID_PRODUK     = "ID_PRODUK";
    public static final String OD_PRODUK_NAMA   = "NAMA_PRODUK";
    public static final String OD_JUMLAH_PESANAN= "jumlah_pesanan";
    public static final String OD_HARGA         = "HARGA";

    public static final String TABLE_HARGA      = "HARGA_SEMEN";
    public static final String HARGA_ID         = "H_ID";
    public static final String HARGA_ID_PRODUK  = "ID_PRODUK";
    public static final String HARGA_JUMLAH     = "JUMLAH";
    public static final String HARGA_HARGA      = "HARGA";
    public static final String HARGA_NAMA_PRODUK= "NAMA_PRODUK";

    public static final String TABLE_GUDANG     = "GUDANG";
    public static final String GUDANG_ID        = "G_ID";
    public static final String GUDANG_KODE      = "KODE";
    public static final String GUDANG_NAMA      = "NAMA";
    public static final String GUDANG_DEFAULT   = "default_gudang"; //non kapital sesuai di api

    public static final String TABLE_KEGIATAN   = "KEGIATAN";
    public static final String KEGIATAN_ID      = "KEGIATAN_ID";
    public static final String KEGIATAN_KUNJUNGAN_ID    = "KUNJUNGAN_ID";
    public static final String KEGIATAN_KETERANGAN      = "KEGIATAN_KETERANGAN";


    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        String tb_toko ="create table "+TABLE_TOKO+
                " ("+ TOKO_ID+" integer primary key, "+
                TOKO_KODE+" text,"+
                TOKO_NAMA+" text,"+
                TOKO_ALAMAT+" text)";

        String tb_harga_semen ="create table "+TABLE_HARGA+
                " ("+ HARGA_ID+" integer primary key, "+
                HARGA_ID_PRODUK+" text,"+
                HARGA_JUMLAH+" int,"+
                HARGA_HARGA+" int,"+
                HARGA_NAMA_PRODUK+" text)";

        String tb_gudang ="create table "+TABLE_GUDANG+
                " ("+ GUDANG_ID+" integer primary key, "+
                GUDANG_KODE+" text,"+
                GUDANG_NAMA+" text)";

        String tb_kunjungan ="create table "+TABLE_KUNJUNGAN+
                " ("+ KUNJUNGAN_ID+" integer primary key, "+
                KUNJUNGAN_KODE+" text,"+
                KUNJUNGAN_NAMA_TOKO+" text,"+
                KUNJUNGAN_LATITUDE+" int,"+
                KUNJUNGAN_LONGITUDE+" int,"+
                KUNJUNGAN_TANGGAL+" text,"+
                KUNJUNGAN_JAM_MULAI+" text,"+
                KUNJUNGAN_JAM_SELESAI+" text,"+
                KUNJUNGAN_IS_BARCODE+" int,"+
                KUNJUNGAN_IMAGE_PATH+" text)";

        String tb_order ="create table "+TABLE_ORDER+
                " ("+ ORDER_ID+" integer primary key, "+
                ORDER_K_ID+" int,"+
                ORDER_KODE_TOKO+" text,"+
                ORDER_TOTAL_HARGA+" text,"+
                ORDER_GUDANG+" text,"+
                ORDER_GUDANG_NAMA+" text,"+
                ORDER_IS_APPROVAL+" int,"+
                ORDER_JENIS_PENGIRIMAN+" text,"+
                ORDER_TANGGAL_KIRIM+" text,"+
                ORDER_TANGGAL_KIRIM_SHOW+" text)";

        String tb_order_detail ="create table "+TABLE_ORDER_DETAIL+
                " ("+ OD_ID+" integer primary key, "+
                OD_ORDER_ID+" int,"+
                OD_ID_PRODUK+" text,"+
                OD_PRODUK_NAMA+" text,"+
                OD_JUMLAH_PESANAN+" int,"+
                OD_HARGA+" int)";

        String tb_kegiatan ="create table "+TABLE_KEGIATAN+
                " ("+ KEGIATAN_ID+" integer primary key, "+
                KEGIATAN_KUNJUNGAN_ID+" int,"+
                KEGIATAN_KETERANGAN+" text)";


        db.execSQL(tb_toko);
        db.execSQL(tb_harga_semen);
        db.execSQL(tb_gudang);
        db.execSQL(tb_kunjungan);
        db.execSQL(tb_order);
        db.execSQL(tb_order_detail);
        db.execSQL(tb_kegiatan);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_TOKO);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_HARGA);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_GUDANG);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_KUNJUNGAN);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_ORDER);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_ORDER_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_KEGIATAN);
        onCreate(db);
    }

    public void dropAlltable(){
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DROP TABLE " + TABLE_TOKO);
        db.execSQL("DROP TABLE " + TABLE_HARGA);
        db.execSQL("DROP TABLE " + TABLE_GUDANG);
        db.execSQL("DROP TABLE " + TABLE_KUNJUNGAN);
        db.execSQL("DROP TABLE " + TABLE_ORDER);
        db.execSQL("DROP TABLE " + TABLE_ORDER);
        db.execSQL("DROP TABLE " + TABLE_KEGIATAN);
    }
    public  void backup(){
        try {
            File file = new File(DB_PATH + DB_NAME); //Uri.toString());
            //FileInputStream myInpsut = FileInputStream(file); // myContext.getAssets().open(DB_NAME);
            FileInputStream myInput;

            myInput = new FileInputStream(file);


            // Path to the just created empty db
            String outFileName = "/sdcard/"+DB_NAME; // DB_PATH + DB_NAME;

            //Open the empty db as the output stream
            OutputStream myOutput = new FileOutputStream(outFileName);

            //transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer))>0){
                myOutput.write(buffer, 0, length);
            }

            //Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    public boolean insertToko (String kode, String nama,
                                     String alamat)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("" + TOKO_KODE, kode);
        contentValues.put("" + TOKO_NAMA, nama);
        contentValues.put("" + TOKO_ALAMAT, alamat);

        db.insert("" + TABLE_TOKO, null, contentValues);
        return true;
    }
    public boolean insertHarga (String id_produk, int jumlah,
                               int harga, String nama_produk)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("" + HARGA_ID_PRODUK, id_produk);
        contentValues.put("" + HARGA_JUMLAH, jumlah);
        contentValues.put("" + HARGA_HARGA, harga);
        contentValues.put("" + HARGA_NAMA_PRODUK, nama_produk);

        db.insert("" + TABLE_HARGA, null, contentValues);
        return true;
    }
    public boolean insertGudang (String kode, String nama)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("" + GUDANG_KODE, kode);
        contentValues.put("" + GUDANG_NAMA, nama);

        db.insert("" + TABLE_GUDANG, null, contentValues);
        return true;
    }
    public boolean insertKunjungan (String kode, String nama_toko, int latitude, int longitude,
                                    String tanggal, String jam_mulai, String jam_selesai,
                                    int is_barcode, String image_path)
    {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("" + KUNJUNGAN_KODE, kode);
        contentValues.put("" + KUNJUNGAN_NAMA_TOKO, nama_toko);
        contentValues.put("" + KUNJUNGAN_LATITUDE, latitude);
        contentValues.put("" + KUNJUNGAN_LONGITUDE, longitude);
        contentValues.put("" + KUNJUNGAN_TANGGAL, tanggal);
        contentValues.put("" + KUNJUNGAN_JAM_MULAI, jam_mulai);
        contentValues.put("" + KUNJUNGAN_JAM_SELESAI, jam_selesai);
        contentValues.put("" + KUNJUNGAN_IS_BARCODE, is_barcode);
        contentValues.put("" + KUNJUNGAN_IMAGE_PATH, image_path);

        db.insert("" + TABLE_KUNJUNGAN, null, contentValues);
        return true;
    }
    public boolean insertOrder (int kunjungan_id, String kode_toko, String total_harga, String gudang,
                                    String gudang_nama, int is_approval, String jenis_pengiriman,
                                    String tgl_kirim, String tgl_kirim_show)
    {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("" + ORDER_K_ID, kunjungan_id);
        contentValues.put("" + ORDER_KODE_TOKO, kode_toko);
        contentValues.put("" + ORDER_TOTAL_HARGA, total_harga);
        contentValues.put("" + ORDER_GUDANG, gudang);
        contentValues.put("" + ORDER_GUDANG_NAMA, gudang_nama);
        contentValues.put("" + ORDER_IS_APPROVAL, is_approval);
        contentValues.put("" + ORDER_JENIS_PENGIRIMAN, jenis_pengiriman);
        contentValues.put("" + ORDER_TANGGAL_KIRIM, tgl_kirim);
        contentValues.put("" + ORDER_TANGGAL_KIRIM_SHOW, tgl_kirim_show);

        db.insert("" + TABLE_ORDER, null, contentValues);
        return true;
    }
    public boolean insertOrderDetail (int order_id, String id_produk, String produk_nama,
                                      int jumlah_pesanan,int harga)
    {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("" + OD_ORDER_ID, order_id);
        contentValues.put("" + OD_ID_PRODUK, id_produk);
        contentValues.put("" + OD_PRODUK_NAMA, produk_nama);
        contentValues.put("" + OD_JUMLAH_PESANAN, jumlah_pesanan);
        contentValues.put("" + OD_HARGA, harga);

        db.insert("" + TABLE_ORDER_DETAIL, null, contentValues);
        return true;
    }
    public boolean insertKegiatan (int kunjungan_id, String keterangan)
    {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("" + KEGIATAN_KUNJUNGAN_ID, kunjungan_id);
        contentValues.put("" + KEGIATAN_KETERANGAN, keterangan);

        db.insert("" + TABLE_KEGIATAN, null, contentValues);
        return true;
    }
    public ArrayList<String> getTokoById(int m_id){
        ArrayList<String> array_list = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery("select * from " + TABLE_TOKO + " where " + TOKO_ID + "=" + m_id + "", null);
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex(TOKO_ID)));
            array_list.add(res.getString(res.getColumnIndex(TOKO_KODE)));
            array_list.add(res.getString(res.getColumnIndex(TOKO_NAMA)));
            array_list.add(res.getString(res.getColumnIndex(TOKO_ALAMAT)));
            res.moveToNext();
        }
        return array_list;
    }
    public ArrayList<HashMap<String, String>> getKunjunganById(int id){
        ArrayList<String> array_list = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_KUNJUNGAN + " " +
                "where " + KUNJUNGAN_ID + "=" + id + "", null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getOrderByKunjunganID(int id){
        ArrayList<String> array_list = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_ORDER + " " +
                "where " + ORDER_K_ID + "=" + id + "", null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getDetailOrderByOrderId(int id){
        ArrayList<String> array_list = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_ORDER_DETAIL + " " +
                "where " + OD_ORDER_ID + "=" + id + "", null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getKegiatanByKunjunganID(int id){
        ArrayList<String> array_list = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_KEGIATAN + " " +
                "where " + KEGIATAN_KUNJUNGAN_ID + "=" + id + "", null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getOrderByKunjungan(int kunjungan_id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_ORDER +
                        " where " + ORDER_K_ID + "=" + kunjungan_id + " order by "+ORDER_ID+" ASC"
                , null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getKegiatanByKunjungan(int kunjungan_id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_KEGIATAN +
                        " where " + KEGIATAN_KUNJUNGAN_ID + "=" + kunjungan_id + " order by "+KEGIATAN_ID+" ASC"
                , null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getDetailOrderByOrder(int order_id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_ORDER_DETAIL +
                        " where " + OD_ORDER_ID + "=" + order_id + " order by "+OD_ID+" ASC"
                , null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getAllToko()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_TOKO +
                        " order by "+TOKO_NAMA+" ASC"
                , null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getAllHarga()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_HARGA +
                        " order by "+HARGA_ID+" ASC"
                , null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getAllGudang()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_GUDANG +
                        " order by "+GUDANG_ID+" ASC"
                , null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getAllKunjungan()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_KUNJUNGAN +
                        " order by "+KUNJUNGAN_ID+" ASC"
                , null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public ArrayList<HashMap<String, String>> getLimitToko(int offset)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select * from " + TABLE_TOKO +
                        " order by "+TOKO_ID+" DESC limit "+LIMIT+" offset " +offset
                , null);
        cursor.moveToFirst();

        ArrayList<HashMap<String, String>> maplist = new ArrayList<HashMap<String, String>>();

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                for(int i=0; i<cursor.getColumnCount();i++)
                {
                    map.put(cursor.getColumnName(i), cursor.getString(i));
                }

                maplist.add(map);
            } while (cursor.moveToNext());
        }
        return maplist;
    }
    public int getLastIDKunjungan()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        int kunjungan_last_id = 0;
        String query = "SELECT "+KUNJUNGAN_ID+" from "+TABLE_KUNJUNGAN+" order by "+KUNJUNGAN_ID+" DESC limit 1";
        Cursor c = db.rawQuery(query, null);
        if (c != null && c.moveToFirst()) {
            kunjungan_last_id = c.getInt(0);
            //The 0 is the column index, we only have 1 column, so the index is 0
        }
        //Log.d("kunjungan last id : ", ""+kunjungan_last_id);
        return kunjungan_last_id;
    }
    public int getLastIDOrder()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        int kunjungan_last_id = 0;
        String query = "SELECT "+ORDER_ID+" from "+TABLE_ORDER+" order by "+ORDER_ID+" DESC limit 1";
        Cursor c = db.rawQuery(query, null);
        if (c != null && c.moveToFirst()) {
            kunjungan_last_id = c.getInt(0);
            //The 0 is the column index, we only have 1 column, so the index is 0
        }
        //Log.d("kunjungan last id : ", ""+kunjungan_last_id);
        return kunjungan_last_id;
    }
    public boolean deleteTokoById(int t_id){
        SQLiteDatabase db = this.getWritableDatabase();
        if( db.delete(TABLE_TOKO+"",
                TOKO_ID+" =  "+t_id,
                null) ==1)
            return true;
        else
            return false;
    }
    public void deleteKunjunganById(int k_id, int order_id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_KUNJUNGAN+"", KUNJUNGAN_ID+" =  "+k_id, null);
        db.delete(TABLE_KEGIATAN+"", KEGIATAN_KUNJUNGAN_ID+" =  "+k_id, null);
        db.delete(TABLE_ORDER+"", ORDER_K_ID+" =  "+k_id, null);
        db.delete(TABLE_ORDER_DETAIL+"", OD_ORDER_ID+" =  "+order_id, null);
    }
    public void setEmptyDB(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_TOKO);
        db.execSQL("delete from "+ TABLE_KUNJUNGAN);
        db.execSQL("delete from "+ TABLE_ORDER);
        db.execSQL("delete from "+ TABLE_ORDER_DETAIL);
        db.execSQL("delete from "+ TABLE_KEGIATAN);
        db.execSQL("delete from "+ TABLE_HARGA);
        db.execSQL("delete from "+ TABLE_GUDANG);
    }
    public int totalNotifikasi(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_TOKO);
        return numRows;
    }
    public int totalKunjungan(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_KUNJUNGAN);
        return numRows;
    }
    public int totalOrder(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_ORDER);
        return numRows;
    }
    public int totalDetailOrder(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_ORDER_DETAIL);
        return numRows;
    }
    public int totalKegiatan(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_KEGIATAN);
        return numRows;
    }
    public int totalOrderByKunjungan(int kunjungan_id){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_ORDER,
                ORDER_K_ID+"=?", new String[] {""+kunjungan_id});
        return numRows;
    }
    public int totalKegiatanByKunjungan(int kunjungan_id){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_KEGIATAN,
                KEGIATAN_KUNJUNGAN_ID+"=?", new String[] {""+kunjungan_id});
        return numRows;
    }
    public void updateJamSelesaiKunjungan(int kunjungan_id, String jam_selesai){
        SQLiteDatabase db = this.getReadableDatabase();
        String update_tgl_selesai = "UPDATE "+TABLE_KUNJUNGAN+" " +
                "SET "+KUNJUNGAN_JAM_SELESAI+"='"+jam_selesai+"' " +
                "WHERE "+KUNJUNGAN_ID+"="+kunjungan_id+" ";
        Log.d("sql tgl selesai : ", update_tgl_selesai);
        db.execSQL(update_tgl_selesai);
    }
}
