package com.legendjava.apwsales;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.legendjava.apwsales.app.MyAppController;
import com.legendjava.apwsales.config.GlobalConfig;
import com.legendjava.apwsales.db.DBHelper;
import com.legendjava.apwsales.model.Produk;
import com.legendjava.apwsales.mysp.ObscuredSharedPreferences;
import com.legendjava.apwsales.util.MyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by imamudin on 10/05/18.
 */

public class FormOrderOffline extends AppCompatActivity {
    LinearLayout ll_main, ll_order_produk;
    EditText et_tanggal_kirim, et_total, et_tanggal;
    Button btn_hapus, btn_simpan;
    ObscuredSharedPreferences pref;
    Double total_piutang;
    ProgressDialog loading;
    Spinner sp_gudang;
    HashMap<String,String> gudangMap;
    JsonObjectRequest request;
    String default_gudang;
    MyLog myLog;
    DBHelper mydb;

    RadioGroup radiogrup;
    RadioButton radiobutton;
    boolean ada_faktur = true;
    boolean isCheck = false;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_order_offline);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("ORDER SEMEN");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(FormOrderOffline.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        mydb = new DBHelper(FormOrderOffline.this);

        ll_main         = (LinearLayout)findViewById(R.id.ll_main);
        ll_order_produk = (LinearLayout)findViewById(R.id.ll_order_produk);

        et_tanggal_kirim= (EditText) findViewById(R.id.et_tanggal_kirim);
        et_tanggal      = (EditText) findViewById(R.id.et_tanggal);
        et_total        = (EditText) findViewById(R.id.et_total);
        sp_gudang       = (Spinner)findViewById(R.id.sp_gudang);

        radiogrup       = (RadioGroup) findViewById(R.id.radio);

        Intent old  = getIntent();
        ada_faktur      = old.getBooleanExtra(GlobalConfig.GADA_FAKTUR,true);
        total_piutang   = old.getDoubleExtra(GlobalConfig.GTOKO_TOTAL_PIUTANG, 0);
        default_gudang  = pref.getString(DBHelper.GUDANG_DEFAULT, "");

        ArrayList<HashMap<String, String>> produks = mydb.getAllHarga();

        HashMap<String, String> temp = new HashMap<String, String>();
        for(int i=0; i<produks.size();i++){
            temp    = produks.get(i);
            Produk produk   = new Produk();
            produk.HARGA    = Integer.parseInt(temp.get(DBHelper.HARGA_HARGA));
            produk.NAMA_PRODUK= temp.get(DBHelper.HARGA_NAMA_PRODUK);
            produk.ID_PRODUK= temp.get(DBHelper.HARGA_ID_PRODUK);

            add_form_produk(produk);
        }

        ArrayList<HashMap<String, String>> gudangs = mydb.getAllGudang();
        gudangMap = new HashMap<String, String>();

        List<String> gudanglist = new ArrayList<String>();
        String nama_defaul_gudang = "";
        for(int i=0; i<gudangs.size();i++){
            temp    = gudangs.get(i);
            gudanglist.add(temp.get(DBHelper.GUDANG_NAMA));
            gudangMap.put(temp.get(DBHelper.GUDANG_NAMA), temp.get(DBHelper.GUDANG_KODE));

            //untuk mencari defaul gudang
            if(temp.get(DBHelper.GUDANG_KODE).equals(default_gudang)){
                nama_defaul_gudang = temp.get(DBHelper.GUDANG_NAMA);
            }
        }
        //gudang
        ArrayAdapter<String> gudangAdapater = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, gudanglist);
        gudangAdapater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_gudang.setAdapter(gudangAdapater);
        if(!nama_defaul_gudang.equals("")){
            int position_defaul = gudangAdapater.getPosition(nama_defaul_gudang);
            sp_gudang.setSelection(position_defaul);
        }

        btn_hapus       = (Button)findViewById(R.id.btn_hapus);
        btn_simpan      = (Button)findViewById(R.id.btn_simpan);

        et_tanggal.setOnClickListener(btnClick);
        btn_hapus.setOnClickListener(btnClick);
        btn_simpan.setOnClickListener(btnClick);
    }
    private void add_form_produk(Produk produk){
        LayoutInflater inflater = (LayoutInflater)this.getLayoutInflater();
        View stok_view          = inflater.inflate(R.layout.list_form_order, null);

        final TextView tv_produk_id     =(TextView)stok_view.findViewById(R.id.tv_produk_id);
        final TextView tv_nama_semen    =(TextView)stok_view.findViewById(R.id.tv_nama_semen);
        final EditText et_harga_semen   =(EditText)stok_view.findViewById(R.id.et_harga_semen);
        final EditText et_jumlah        =(EditText)stok_view.findViewById(R.id.et_jumlah_pesanan);
        final TextView tv_harga_semen_dec=(TextView)stok_view.findViewById(R.id.tv_harga_semen_decimal);

        et_jumlah.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
                et_total.setText(decimalToRupiah(getTotal()));
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        tv_nama_semen.setText(produk.NAMA_PRODUK);
        et_harga_semen.setText(""+produk.HARGA);
        tv_harga_semen_dec.setText(""+produk.HARGA);
        tv_produk_id.setText(produk.ID_PRODUK);

        et_harga_semen.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
                et_total.setText(decimalToRupiah(getTotal()));
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        ll_order_produk.addView(stok_view);
    }
    private double getTotal(){
        Double total =0.0;
        for(int i=0;i<ll_order_produk.getChildCount();i++){
            LinearLayout layer0     = (LinearLayout) ll_order_produk.getChildAt(i);

            //semua ini melihat struktur pada list_form_order.xml
            //untuk mencari jumlah pesanan
            LinearLayout layer11    = (LinearLayout) layer0.getChildAt(1);
            LinearLayout layer111   = (LinearLayout) layer11.getChildAt(1);
            EditText et_jumlah_pesan= (EditText) layer111.getChildAt(2);

            String jumlah_pesan = et_jumlah_pesan.getText().toString().trim();
            if(jumlah_pesan.equals("")) jumlah_pesan ="0";

            //untuk mencari harga semen
            LinearLayout layer01    = (LinearLayout) layer0.getChildAt(0);
            LinearLayout layer011   = (LinearLayout) layer01.getChildAt(1);
            EditText tv_harga_dec   = (EditText) layer011.getChildAt(1);        //karena et harga pada array indek ke 2

            String harga = tv_harga_dec.getText().toString().trim();
            if(harga.equals("")) harga ="0";

            total += Double.parseDouble(harga) * Double.parseDouble(jumlah_pesan);
        }
        return total;
    }
    private boolean isPesananPenuh(){
        int total_kosong    = 0;
        for(int i=0;i<ll_order_produk.getChildCount();i++){
            LinearLayout layer0     = (LinearLayout) ll_order_produk.getChildAt(i);
            LinearLayout layer01    = (LinearLayout) layer0.getChildAt(1);
            LinearLayout layer011   = (LinearLayout) layer01.getChildAt(1);

            EditText et_jumlah_pesan= (EditText) layer011.getChildAt(2);

            if(et_jumlah_pesan.getText().length()<=0 && et_jumlah_pesan.getText().toString().equals("0")){
                total_kosong++;
            }
        }
        if(total_kosong==ll_order_produk.getChildCount()){
            return false;
        }else{
            return true;
        }
    }
    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==btn_simpan){
                if(getTotal()>0 && et_tanggal.getText().length()>0 && isPesananPenuh()){
                    kirimOrder(1);
                    //cekSelisih();
                }else{
                    notifikasi("Silakan lengkapi semua form.");
                }
            }else if(v==btn_hapus){
                et_total.setText("");
                et_tanggal.setText("");
                return;
            }else if(v==et_tanggal){
                dateTimePick();
                return;
            }
        }
    };
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 2, 1, "Hapus Filter").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    private void cekSelisih(){
        double total_harga = getTotal();
        double selisih = Double.parseDouble(pref.getString(GlobalConfig.GREAL_PLAFON,"0"))-(total_piutang + total_harga);

        if(selisih >= 0){
            kirimOrder(1);
        }else{
            //tidak bisa order
            String message  = "Total tagihan   = "+decimalToRupiah((total_piutang + total_harga))+""+System.getProperty("line.separator");
            message        += "Maksimum Plafon = "+decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GREAL_PLAFON,"0")))+System.getProperty("line.separator")+System.getProperty("line.separator");
            message        += "Order pelanggan selanjutnya menunggu approval dari perusahaan, Apakah anda melanjutkan proses order?";

            AlertDialog.Builder builder = new AlertDialog.Builder(FormOrderOffline.this);
            builder.setTitle("")
                    .setCancelable(false)
                    .setMessage(message)
                    .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            notifikasi_dialog("","Order dibatalkan.");
                        }
                    }).setPositiveButton("Lanjutkan", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    kirimOrder(0);
                }
            });;
            AlertDialog dialog = builder.create();
            dialog.show();

        }
    }
    private void kirimOrder(int is_approval){
        if(!ada_faktur){
            is_approval =  0;
        }
        //menyimpan order ke database local
        double total_harga = getTotal();

        String gudang_nama = sp_gudang.getSelectedItem().toString();
        String gudang_id = gudangMap.get(gudang_nama);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            final JSONArray json_id_produk  = new JSONArray();
            final JSONArray json_nama_produk= new JSONArray();
            final JSONArray json_jumlah     = new JSONArray();
            final JSONArray json_harga      = new JSONArray();

            boolean status_ada_harga_nol = false;
            for(int i=0;i<ll_order_produk.getChildCount();i++){
                LinearLayout layer0     = (LinearLayout) ll_order_produk.getChildAt(i);

                //semua ini melihat struktur pada list_form_order.xml
                //untuk mencari jumlah dan id produk
                LinearLayout layer11    = (LinearLayout) layer0.getChildAt(1);
                LinearLayout layer111   = (LinearLayout) layer11.getChildAt(1);
                EditText et_jumlah_pesan= (EditText) layer111.getChildAt(2);

                TextView tv_produk_id   = (TextView) layer111.getChildAt(4);

                //untuk mencari harga
                LinearLayout layer01    = (LinearLayout) layer0.getChildAt(0);
                LinearLayout layer011   = (LinearLayout) layer01.getChildAt(1);
                TextView tv_nama_produk = (TextView) layer011.getChildAt(0);        //karena et harga pada array indek ke 2
                EditText tv_harga_dec   = (EditText) layer011.getChildAt(1);        //karena et harga pada array indek ke 2

                String s_harga = tv_harga_dec.getText().toString().trim();
                if(s_harga.equals("")) s_harga ="0";

                double harga = Integer.parseInt(s_harga);

                json_id_produk.put(i,tv_produk_id.getText().toString());
                json_harga.put(i,harga);
                json_nama_produk.put(i,tv_nama_produk.getText().toString());

                String jumlah_pesan = et_jumlah_pesan.getText().toString();
                if(jumlah_pesan.equals("")){
                    jumlah_pesan="0";
                }
                json_jumlah.put(i,Integer.parseInt(jumlah_pesan));

                //untuk mengecek apabila ada harga yang diisi nol dan jumlahnya lebih dari nol
                if(harga <= 0 && Integer.parseInt(jumlah_pesan) > 0){
                    status_ada_harga_nol = true;
                }
            }

            if(status_ada_harga_nol){
                notifikasi("Silakan isi harga semen!");
            }else {
                if(isCheck) {
                    //untuk jenis pengiriman
                    int selectedId = radiogrup.getCheckedRadioButtonId();
                    // find the radiobutton by returned id
                    radiobutton = (RadioButton) findViewById(selectedId);

                    //simpan data order
                    mydb.insertOrder(pref.getInt(DBHelper.KUNJUNGAN_LAST_ID, 0),
                            pref.getString(GlobalConfig.ID_TOKO, ""),
                            "" + total_harga, gudang_id, gudang_nama, is_approval,
                            radiobutton.getText().toString(),
                            et_tanggal_kirim.getText().toString().trim(),
                            et_tanggal.getText().toString().trim());

                    int lastIDOrder = mydb.getLastIDOrder();
                    //simpan data detail order
                    for (int i = 0; i < json_id_produk.length(); i++) {
                        mydb.insertOrderDetail(lastIDOrder, json_id_produk.getString(i),
                                json_nama_produk.getString(i),
                                json_jumlah.getInt(i), json_harga.getInt(i));
                    }

                    dialogNotifikasi("Data Order berhasil disimpan sementara." +
                            "\nSilakan melakukan sinkronisasi saat tersambung dengan internet." +
                            "\nTerima kasih.");
                }else {
                    isCheck = true;
                    notifikasi_dialog(getResources().getString(R.string.app_name),
                                "Silakan melakukan pengecekan kembali sebelum data disimpan."+
                                    "\nTerima kasih.");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }





//        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_SIMPANORDER;
//        Log.d(GlobalConfig.TAG+"/"+getLocalClassName(),""+url);
//
//        double total_harga = getTotal();
//
//        String a_nama = sp_gudang.getSelectedItem().toString();
//        String gudang_id = gudangMap.get(a_nama);
//
//        if(loading == null || !loading.isShowing()) {
//            Log.d(getLocalClassName(), "make progress dialog2");
//            loading = ProgressDialog.show(FormOrderOffline.this, "Mengirim order", "Mohon tunggu...", true);
//        }

        //bisa order
//        JSONObject jsonBody;
//        try {
//            jsonBody = new JSONObject();
//            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
//            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));
//
//            final JSONArray json_id_produk  = new JSONArray();
//            final JSONArray json_jumlah     = new JSONArray();
//            final JSONArray json_harga      = new JSONArray();
//
//            boolean status_ada_harga_nol = false;
//            for(int i=0;i<ll_order_produk.getChildCount();i++){
//                LinearLayout layer0     = (LinearLayout) ll_order_produk.getChildAt(i);
//
//                //semua ini melihat struktur pada list_form_order.xml
//                //untuk mencari jumlah dan id produk
//                LinearLayout layer11    = (LinearLayout) layer0.getChildAt(1);
//                LinearLayout layer111   = (LinearLayout) layer11.getChildAt(1);
//                EditText et_jumlah_pesan= (EditText) layer111.getChildAt(2);
//
//                TextView tv_produk_id   = (TextView) layer111.getChildAt(4);
//
//                //untuk mencari harga
//                LinearLayout layer01    = (LinearLayout) layer0.getChildAt(0);
//                LinearLayout layer011   = (LinearLayout) layer01.getChildAt(1);
//                EditText tv_harga_dec   = (EditText) layer011.getChildAt(1);        //karena et harga pada array indek ke 2
//
//                String s_harga = tv_harga_dec.getText().toString().trim();
//                if(s_harga.equals("")) s_harga ="0";
//
//                double harga = Double.parseDouble(s_harga);
//
//                json_id_produk.put(i,tv_produk_id.getText().toString());
//                json_harga.put(i,harga);
//
//                String jumlah_pesan = et_jumlah_pesan.getText().toString();
//                if(jumlah_pesan.equals("")){
//                    jumlah_pesan="0";
//                }
//                json_jumlah.put(i,Integer.parseInt(jumlah_pesan));
//
//                //untuk mengecek apabila ada harga yang diisi nol dan jumlahnya lebih dari nol
//                if(harga <= 0 && Integer.parseInt(jumlah_pesan) > 0){
//                    status_ada_harga_nol = true;
//                }
//            }
//
//            if(status_ada_harga_nol){
//                notifikasi("Silakan isi harga semen!");
//                loading.dismiss();
//                loading = null;
//            }else {
//                //untuk jenis pengiriman
//                int selectedId = radiogrup.getCheckedRadioButtonId();
//                // find the radiobutton by returned id
//                radiobutton = (RadioButton) findViewById(selectedId);
//
//                jsonBody.put(GlobalConfig.JENIS_PENGIRIMAN, radiobutton.getText().toString());
//                jsonBody.put(GlobalConfig.GPRODUK_ID, json_id_produk);
//                jsonBody.put(GlobalConfig.JUMLAH_PESANAN, json_jumlah);
//                jsonBody.put(GlobalConfig.GHARGA, json_harga);
//
//                jsonBody.put(GlobalConfig.TANGGAL_KIRIM, et_tanggal_kirim.getText().toString().trim());
//                jsonBody.put(GlobalConfig.TOTAL_HARGA, total_harga);
//                jsonBody.put(GlobalConfig.GKODE_GUDANG, gudang_id);
//                jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));
//                jsonBody.put(GlobalConfig.IS_APPROVAL, is_approval);
//
//                myLog.print(GlobalConfig.TAG+"/"+getLocalClassName(), jsonBody.toString());
//                request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        loading.dismiss();
//                        Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "tutup loading");
//                        try {
//                            int status = response.getInt("status");
//                            String message = response.getString("message");
//                            if (status == 1) {
//                                dialogNotifikasi("Data Order berhasil disimpan.\nTerima kasih.");
//                            } else {
//                                notifikasi(message);
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        //Log.d("respons",response.toString());
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        loading.dismiss();
//                        NetworkResponse response = error.networkResponse;
//                        if (response != null && response.data != null) {
//                            Log.d(GlobalConfig.TAG+"/"+getLocalClassName(), "code" + response.statusCode);
//                            switch (response.statusCode) {
//                                case 404:
//                                    displayMessage("Terjadi masalah dengan server.");
//                                    break;
//                                case 408:
//                                    displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
//                                    break;
//                                case 500:
//                                    displayMessage("Terjadi masalah dengan server.");
//                                    break;
//                                default:
//                                    displayMessage("Mohon maaf terjadi kesalahan.");
//                                    break;
//                            }
//                        }
//                    }
//                }) {
//                    public Map<String, String> getHeaders() {
//                        Map<String, String> headers = new Hashtable<String, String>();
//
//                        //Adding parameters
//                        headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
//                        headers.put("Content-Type", "application/json; charset=utf-8");
//                        return headers;
//                    }
//                };
//
//                request.setRetryPolicy(new DefaultRetryPolicy(
//                        GlobalConfig.MY_SOCKET_TIMEOUT_MS,
//                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//                MyAppController.getInstance().addToRequestQueue(request);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//    public void displayMessage(String toastString){
//        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    private void dateTimePick(){
        final View dialogView = View.inflate(FormOrderOffline.this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(FormOrderOffline.this).create();

        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
                GregorianCalendar gc = new GregorianCalendar();
                gc.add(Calendar.DATE, 1);
                long time = gc.getTimeInMillis();
                datePicker.setMinDate(time);

                Calendar c = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth());

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat format_tampil = new SimpleDateFormat("dd MMM yyyy");
                et_tanggal.setText(format_tampil.format(c.getTime()));
                et_tanggal_kirim.setText(format.format(c.getTime()));
                alertDialog.dismiss();
            }});
        alertDialog.setView(dialogView);
        alertDialog.show();
    }
    public void dialogNotifikasi(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(FormOrderOffline.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Intent toko_offline = new Intent(FormOrderOffline.this, TokoOffline.class);
                        startActivity(toko_offline);
                        FormOrderOffline.this.finish();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void notifikasi_dialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(FormOrderOffline.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
}
