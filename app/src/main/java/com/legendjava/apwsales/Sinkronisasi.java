package com.legendjava.apwsales;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.legendjava.apwsales.config.GlobalConfig;
import com.legendjava.apwsales.db.DBHelper;
import com.legendjava.apwsales.listAdapter.ListAdapterKunjungan;
import com.legendjava.apwsales.model.Kunjungan;
import com.legendjava.apwsales.mysp.ObscuredSharedPreferences;
import com.legendjava.apwsales.util.MyLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by imamudin on 12/05/18.
 */

public class Sinkronisasi extends AppCompatActivity {

    List<Kunjungan> list_datas = new ArrayList<Kunjungan>();
    ListView lv_kota;
    ListAdapterKunjungan adapter;
    RequestQueue requestQueue = null;

    ObscuredSharedPreferences pref;
    JsonObjectRequest request =null;
    private SwipeRefreshLayout swipeContainer;
    LinearLayout ll_main, ll_filter;
    MyLog myLog;

    int offSet=0;
    Handler handler;
    Runnable runnable;
    Boolean disableSwipeDown = false;       //untuk mendisable swipe down list view

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_kunjungan);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("SINKRONISASI");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog = new MyLog(getLocalClassName());

        ll_main     =   (LinearLayout)findViewById(R.id.ll_main);
        ll_filter   =   (LinearLayout)findViewById(R.id.ll_filter);


        lv_kota = (ListView)findViewById(R.id.custom_list);
        list_datas.clear();
        adapter = new ListAdapterKunjungan(Sinkronisasi.this, list_datas);
        lv_kota.setAdapter(adapter);

        swipeContainer = (SwipeRefreshLayout)findViewById(R.id.swipeContainer);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                calllist_datas(0);
                Toast.makeText(Sinkronisasi.this,"refresh",Toast.LENGTH_LONG).show();
            }
        });
        lv_kota.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView
                Object o = lv_kota.getItemAtPosition(position);
                Kunjungan kunjungan = (Kunjungan) o;

                Intent i_detailKunjunganOffline = new Intent(Sinkronisasi.this, DetailKunjunganOffline.class);

                //nama pegawai sebagai kunjungan ID
                i_detailKunjunganOffline.putExtra(DBHelper.KUNJUNGAN_ID, kunjungan.NAMA_PEGAWAI);

                startActivityForResult(i_detailKunjunganOffline, GlobalConfig.KODE_SINKRONISASI);
            }
        });
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
                calllist_datas(0);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        lv_kota.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }
            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {
                    if(!disableSwipeDown) {
                        swipeContainer.setRefreshing(true);
                        handler = new Handler();

                        runnable = new Runnable() {
                            public void run() {
                                calllist_datas(offSet);
                            }
                        };
                        //untuk menerlambatkan 0 detik
                        handler.postDelayed(runnable, 000);
                    }else{
                        //Toast.makeText(Sinkronisasi.this,"Data telah ditampilkan semua.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        ll_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent filter = new Intent(Sinkronisasi.this, Filter.class);
                startActivityForResult(filter, GlobalConfig.KODE_INTENT_FILTER_KUNJUNGAN);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case GlobalConfig.KODE_INTENT_FILTER_KUNJUNGAN:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        calllist_datas(0);
                        break;
                }
                break;
            case GlobalConfig.KODE_SINKRONISASI:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        calllist_datas(0);
                        break;
                }
                break;
        }
    }
    private void calllist_datas(int page){
        if(page==0){
            list_datas.clear();
            adapter.notifyDataSetChanged();
            offSet=0;
            disableSwipeDown = false;
        }
        swipeContainer.setRefreshing(true);

        DBHelper mydb = new DBHelper(Sinkronisasi.this);
        mydb = new DBHelper(Sinkronisasi.this);
        ArrayList<HashMap<String, String>> kunjungans = mydb.getAllKunjungan();

        if(kunjungans.size()<DBHelper.LIMIT){         //50 dari jumlah limit di web
            disableSwipeDown = true;
        }
        HashMap<String, String> temp = new HashMap<String, String>();
        SimpleDateFormat jam_format = new SimpleDateFormat("HH:mm:SS");
        for(int i=0; i<kunjungans.size();i++){
            temp    = kunjungans.get(i);

            int durasi_menit = 0;
            myLog.print("jam mulai "+temp.get(DBHelper.KUNJUNGAN_JAM_MULAI));
            myLog.print("jam selesai "+temp.get(DBHelper.KUNJUNGAN_JAM_SELESAI));
            myLog.print("data "+temp.toString());
            try {
                Date jam_mulai  = jam_format.parse(temp.get(DBHelper.KUNJUNGAN_JAM_MULAI));
                Date jam_selesai= jam_format.parse(temp.get(DBHelper.KUNJUNGAN_JAM_SELESAI));

                long mills = jam_selesai.getTime() - jam_mulai.getTime();
                durasi_menit = (int)(mills/(1000*60)) % 60;

            } catch (ParseException e) {
                e.printStackTrace();
            }

            Kunjungan iData = new Kunjungan(temp.get(DBHelper.KUNJUNGAN_ID),
                "", temp.get(DBHelper.TOKO_KODE),
                    temp.get(DBHelper.KUNJUNGAN_NAMA_TOKO), temp.get(DBHelper.KUNJUNGAN_TANGGAL),
                    temp.get(DBHelper.KUNJUNGAN_JAM_MULAI), temp.get(DBHelper.KUNJUNGAN_JAM_SELESAI),
                    durasi_menit+" menit");

            // adding news to news array
            list_datas.add(iData);
            adapter.notifyDataSetChanged();

        }
        swipeContainer.setRefreshing(false);
        if(kunjungans.size()<=0){
            notifikasi("Tidak ada Kunjungan Offline");
        }
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        //getMenuInflater().inflate(R.menu.menu_pelanggaran_santri, menu);
        return true;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}