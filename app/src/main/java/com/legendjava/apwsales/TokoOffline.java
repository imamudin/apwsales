package com.legendjava.apwsales;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.legendjava.apwsales.config.GlobalConfig;
import com.legendjava.apwsales.db.DBHelper;
import com.legendjava.apwsales.mysp.ObscuredSharedPreferences;
import com.legendjava.apwsales.util.MyLog;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.legendjava.apwsales.Dashboard.REQUEST_IMAGE_CAPTURE;
import static com.legendjava.apwsales.db.DBHelper.KUNJUNGAN_JAM_SELESAI;

/**
 * Created by imamudin on 08/05/18.
 */

public class TokoOffline extends AppCompatActivity {
    LinearLayout ll_main;
    ObscuredSharedPreferences pref;
    ProgressDialog loading;
    JsonObjectRequest request;
    MyLog myLog;
    ImageView img_toko;
    Button btn_order, btn_kegiatan, btn_selesai;

    TextView tv_nama_toko, tv_alamat_toko;

    DBHelper mydb;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 99;

    //untuk membuat folder foto
    public String pictureImagePath="";

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tokooffline);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Data Pelanggan");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref        = new ObscuredSharedPreferences(TokoOffline.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );
        myLog       = new MyLog(getLocalClassName());

        img_toko    = (ImageView)findViewById(R.id.img_toko);
        btn_kegiatan     = (Button)findViewById(R.id.btn_kegiatan);
        btn_selesai      = (Button)findViewById(R.id.btn_selesai);

        tv_nama_toko    = (TextView)findViewById(R.id.tv_nama_toko);
        tv_alamat_toko  = (TextView)findViewById(R.id.tv_alamat_toko);

        tv_nama_toko.setText(pref.getString(GlobalConfig.GTOKO_NAMA,""));
        tv_alamat_toko.setText(pref.getString(GlobalConfig.GTOKO_ALAMAT,""));

        btn_kegiatan.setOnClickListener(btnClick);
        btn_selesai.setOnClickListener(btnClick);

        ll_main             = (LinearLayout)findViewById(R.id.ll_main);
        mydb = new DBHelper(TokoOffline.this);

        btn_order = (Button)findViewById(R.id.btn_order);
        btn_order.setOnClickListener(btnClick);

        myLog.print(pref.getString(DBHelper.KUNJUNGAN_IMAGE_PATH, ""));
        if(pref.getString(DBHelper.KUNJUNGAN_IMAGE_PATH, "").equals("")) {
            //langsung ambil gambar dari kamera
            startCamera();
        }else {
            setPictureImageFromPath(pref.getString(DBHelper.KUNJUNGAN_IMAGE_PATH, ""));
        }

        //tes
        myLog.print("total order by kunjungan : "+mydb.totalOrderByKunjungan(pref.getInt(DBHelper.KUNJUNGAN_LAST_ID, 0)));
        myLog.print("total order : "+mydb.totalOrder());
        myLog.print("total detail order : "+mydb.totalDetailOrder());
        myLog.print("k_id : "+pref.getInt(DBHelper.KUNJUNGAN_LAST_ID, 0));
    }
    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent open = null;
            //untuk mengecek toko sudah terpilih atau belum
            if(v==btn_order){
                //mengecek kamera dan toko tidak boleh kosong
                if(!pref.getBoolean(GlobalConfig.IS_TOKO_OFFLINE, false)) {
                    notifikasi("Silakan ambil gambar untuk melalukan order.");
                    return;
                }else {
                    //tes
                    if(mydb.totalOrderByKunjungan(pref.getInt(DBHelper.KUNJUNGAN_LAST_ID, 0)) >=1){
                        notifikasi("Anda sudah melalukan order.");
                        return;
                    }else {
                        open = new Intent(TokoOffline.this, FormOrderOffline.class);
                    }
                }
            }else if (v == btn_selesai){
                onBackPressed();
                return;
            }else  if(v == btn_kegiatan){
                if(!pref.getBoolean(GlobalConfig.IS_TOKO_OFFLINE, false)) {
                    notifikasi("Silakan ambil gambar untuk menambahkan kegiatan.");
                    return;
                }else {
                    if(mydb.totalKegiatanByKunjungan(pref.getInt(DBHelper.KUNJUNGAN_LAST_ID, 0)) >=1){
                        notifikasi("Anda sudah menambahkan kegiatan.");
                    }else {
                        tambahKegiatan();
                    }
                }
                return;
            }

            if(open != null){
                startActivity(open);
            }else{
                notifikasi("Mohon maaf terjadi kesalahan.");
            }
        }
    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        //untuk menangkap image dari kamera
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if(resultCode == RESULT_OK) {
                myLog.print("Camera Result == OK");
                pref.edit().putString(DBHelper.KUNJUNGAN_IMAGE_PATH, pictureImagePath).commit();
                setPictureImageFromPath(pictureImagePath);
            }else{
                myLog.print("Camera Result != OK");
                openDashboard();
            }
        }
    }
    private void tambahKegiatan(){
        LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater();
        final View promptView = layoutInflater.inflate(R.layout.kegiatan_popup, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TokoOffline.this);
        alertDialogBuilder.setView(promptView);

        final EditText et_kegiatan  = (EditText) promptView.findViewById(R.id.et_kegiatan);

        alertDialogBuilder.setNegativeButton("BATAL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).setPositiveButton("SIMPAN",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //pengecekan sebelum disimpan
                        String s_kegiatan = et_kegiatan.getText().toString().trim();
                        if(s_kegiatan.length() == 0){
                            notifikasi("Silakan tambahkan kegiatan.");
                        }else {
                            //simpan ke dalam database lokal
                            mydb.insertKegiatan(pref.getInt(DBHelper.KUNJUNGAN_LAST_ID,0),s_kegiatan);
                            notifikasi("Data Kegiatan telah disimpan sementara.\nTerimakasih.");
                            dialog.cancel();
                        }
                    }
                });

        final AlertDialog alert = alertDialogBuilder.create();
        alert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alert.setCancelable(true);
        alert.show();
    }
    //set image from path
    private void setPictureImageFromPath(String path){
        Bitmap bitmapForImage = null;
        File file = new File(path);
        Log.d(GlobalConfig.TAG+"mycopcamera", path);
        if (file.exists()) {
            bitmapForImage = decodeSampledBitmapFromFile(path, 300, 300);
        }
        add_layout_gambar(bitmapForImage, pictureImagePath);
    }
    //untuk permission camera
    private void startCamera(){
        if (ContextCompat.checkSelfPermission(TokoOffline.this,
                android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            //request permission camera
            ActivityCompat.requestPermissions(TokoOffline.this,
                    new String[]{android.Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
        } else {
            checkPermissionWriteExternal();
        }
    }
    //untuk permission write external memory
    private void checkPermissionWriteExternal(){
        int permissionCheck = ContextCompat.checkSelfPermission(TokoOffline.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    TokoOffline.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        } else {
            loadImagefromKamera();
        }
    }
    @Override
    public void onRequestPermissionsResult
            (int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                myLog.print("Camera", "G : " + grantResults[0]);
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    myLog.print(getLocalClassName(), "akses kamera diizinkan");
                    checkPermissionWriteExternal();
                    return;
                } else {
                    myLog.print(getLocalClassName(), "akses kamera tidak diizinkan");
                    //jika tidak dizinkan maka kembali ke Dashboard;
                    openDashboard();
                    return;

//                    //It will return false if the user tapped “Never ask again”.
//                    boolean is_Neveraskagain = ActivityCompat.shouldShowRequestPermissionRationale(this,
//                            android.Manifest.permission.CAMERA);
//                    if (is_Neveraskagain) {
//                        //showAlert();
//                        myLog.print(getLocalClassName(), "is_Neveraskagain false");
//                    } else if (!is_Neveraskagain) {
//                        myLog.print(getLocalClassName(), "is_Neveraskagain true");
//                        if(!pref.getBoolean(GlobalConfig.PERMISIION_CAMERA_FIRST_TIME, false)){
//                            //myLog.print(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : false");
//                            pref.edit().putBoolean(GlobalConfig.PERMISIION_CAMERA_FIRST_TIME, true).commit();
//                        }else{
//                            //myLog.print(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : true");
//                            startInstalledAppDetailsActivity(TokoOffline.this);
//                        }
//                    }
//                    return;
                }
            }
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    loadImagefromKamera();
                }else {
                    openDashboard();
                }
                break;

            default:
                break;
        }
    }
    //untuk membuka intent mengambil foto dari kamera
    private void loadImagefromKamera() {
        createFolder();
        pictureImagePath = getNewImagePath();
        File file = new File(pictureImagePath);
        myScanFile(this.pictureImagePath);
        Uri outputFileUri = Uri.fromFile(file);
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        myLog.print(GlobalConfig.TAG,""+pictureImagePath);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    //membuat nama file baru
    private String getNewImagePath() {
        String folder = Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DCIM + File.separator + GlobalConfig.FOLDER_NAMA;
        pictureImagePath = new File(folder).getAbsolutePath() + File.separator + (new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg");
        File file = new File(pictureImagePath);
        myScanFile(this.pictureImagePath);
        return this.pictureImagePath;
    }
    //membuat folder apw
    private void createFolder() {
        String folder = Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DCIM + File.separator + GlobalConfig.FOLDER_NAMA;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File directory = new File(folder);
            if (!directory.exists()) {
                directory.mkdirs();
                Log.d(GlobalConfig.TAG+"mycopcamera", "" + directory.toString());
                return;
            }
            return;
        }
    }
    //memperkecil ukuran gambar
    public static Bitmap decodeSampledBitmapFromFile(String imagePath, int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(imagePath, options);
    }
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        int min = width;
        if (width > height) {
            int temp = reqHeight;
            reqHeight = reqWidth;
            reqWidth = temp;
        }
        Log.d(GlobalConfig.TAG+"apwcamera1/", "height : " + height + ", width : " + width);
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height;
            int halfWidth = width;
            while (true) {
                if (halfHeight / inSampleSize <= reqHeight && halfWidth / inSampleSize <= reqWidth) {
                    break;
                }
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }
    //menambahkan ke view
    public void add_layout_gambar(Bitmap imageBitmap, final String path){
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());

        img_toko.setImageBitmap(imageBitmap);
        img_toko.setClickable(true);

        //memulai kunjungan karena gambar sudah diambil
        //apabila sudah memulai kunjungan sebelumnya, maka tidak perlu memanggil mulaiKunjungan();
        if(!pref.getBoolean(GlobalConfig.IS_TOKO_OFFLINE, false)) {
            mulaiKunjungan(path);
        }
    }
    //memasukan file kedalam gallery
    private void myScanFile(String path) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.DATA, path);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        getApplication().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }
    private void notifikasi(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snack.setActionTextColor(getResources().getColor(android.R.color.white )).show();
    }
    @Override
    public void onBackPressed()
    {
        if(!pref.getBoolean(GlobalConfig.IS_TOKO_OFFLINE, false)) {
            openDashboard();
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(TokoOffline.this);
            // Set the dialog title
            builder.setTitle(getResources().getString(R.string.app_name))
                    .setMessage("Apakah anda telah selesai melakukan kunjungan?")
                    .setCancelable(false)
                    .setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            tutupKunjungan();
                        }
                    })
                    .setNegativeButton("Belum", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
    private void mulaiKunjungan(String imagePath){
        myLog.print("mulai kunjungan");
        pref.edit().putBoolean(GlobalConfig.IS_TOKO_OFFLINE, true).commit();

        //mencari tanggal dan jam hari ini
        Calendar c_this_day = Calendar.getInstance();
        SimpleDateFormat hari_format= new SimpleDateFormat("yyyy-MM-dd");

        myLog.print("hari : "+hari_format.format(c_this_day.getTime()));
        String s_jam = c_this_day.get(Calendar.HOUR_OF_DAY) + ":" + c_this_day.get(Calendar.MINUTE) + ":" + c_this_day.get(Calendar.SECOND);

        myLog.print("jam : "+s_jam);
        mydb.insertKunjungan (pref.getString(DBHelper.KUNJUNGAN_KODE, ""),
                pref.getString(GlobalConfig.GTOKO_NAMA, ""),
                1, 1,
                hari_format.format(c_this_day.getTime()),
                s_jam,
                "", 0 , imagePath);

        pref.edit().putInt(DBHelper.KUNJUNGAN_LAST_ID, mydb.getLastIDKunjungan()).commit();
    }
    private void tutupKunjungan(){
        //get jam hari ini
        Calendar c_this_day = Calendar.getInstance();
        String s_jam = c_this_day.get(Calendar.HOUR_OF_DAY) + ":" + c_this_day.get(Calendar.MINUTE) + ":" + c_this_day.get(Calendar.SECOND);
        myLog.print("jam selesai : "+s_jam);

        //update jam selesai kunjungan
        mydb.updateJamSelesaiKunjungan(pref.getInt(DBHelper.KUNJUNGAN_LAST_ID, 0), ""+s_jam);


        //menghapus semua preferences
        pref.edit().remove(GlobalConfig.IS_TOKO_OFFLINE).commit();
        pref.edit().remove(DBHelper.KUNJUNGAN_LAST_ID).commit();

        pref.edit().remove(DBHelper.KUNJUNGAN_KODE).commit();
        pref.edit().remove(GlobalConfig.GTOKO_NAMA).commit();
        pref.edit().remove(GlobalConfig.GTOKO_ALAMAT).commit();
        pref.edit().remove(DBHelper.KUNJUNGAN_LATITUDE).commit();
        pref.edit().remove(DBHelper.KUNJUNGAN_LONGITUDE).commit();
        pref.edit().remove(DBHelper.KUNJUNGAN_TANGGAL).commit();
        pref.edit().remove(DBHelper.KUNJUNGAN_JAM_MULAI).commit();
        pref.edit().remove(KUNJUNGAN_JAM_SELESAI).commit();
        pref.edit().remove(DBHelper.KUNJUNGAN_IS_BARCODE).commit();
        pref.edit().remove(DBHelper.KUNJUNGAN_IMAGE_PATH).commit();

        tutupActivity();
    }
    private void tutupActivity(){
        AlertDialog.Builder builder = new AlertDialog.Builder(TokoOffline.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Kunjungan telah selesai.\nTerima kasih.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        openDashboard();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void openDashboard(){
        Intent dashbord = new Intent(TokoOffline.this, Dashboard.class);
        startActivity(dashbord);
        TokoOffline.this.finish();
    }
}
